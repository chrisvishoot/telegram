package com.main;


import com.main.ability.VishootHelloBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class Main {
    public static void main(String[] args) {
        executeTelegramBot();
    }

    public static void executeTelegramBot() {
        // Initialize Api Context
        ApiContextInitializer.init();
        // Instantiate Telegram Bots API
        TelegramBotsApi botsApi = new TelegramBotsApi();
        // Register our bot
        try {
            botsApi.registerBot(new VishootHelloBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }
}

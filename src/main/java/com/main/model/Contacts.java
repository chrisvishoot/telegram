package com.main.model;

public class Contacts {
    private Long reportId;

    private Long numberOfContacts;
    private Long promisedToGo;

    private Long idTelegramUser;
    private String timeStamp;


    public Contacts() {}

    public Long getNumberOfContacts() {
        return numberOfContacts;
    }

    public Contacts setNumberOfContacts(Long numberOfContacts) {
        this.numberOfContacts = numberOfContacts;
        return this;
    }

    public Long getPromisedToGo() {
        return promisedToGo;
    }

    public Contacts setPromisedToGo(Long promisedToGo) {
        this.promisedToGo = promisedToGo;
        return this;
    }

    public Long getIdTelegramUser() {
        return idTelegramUser;
    }

    public Contacts setIdTelegramUser(Long idTelegramUser) {
        this.idTelegramUser = idTelegramUser;
        return this;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public Contacts setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public Long getReportId() {
        return reportId;
    }

    public Contacts setReportId(Long reportId) {
        this.reportId = reportId;
        return this;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "reportId=" + reportId +
                ", numberOfContacts=" + numberOfContacts +
                ", promisedToGo=" + promisedToGo +
                ", idTelegramUser=" + idTelegramUser +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}

package com.main.aws;

import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementAsyncClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;

public class ParameterStore {
    private static ParameterStore instance = null;
    private AWSSimpleSystemsManagement ssm;

    public ParameterStore() {
        this.ssm = AWSSimpleSystemsManagementAsyncClientBuilder.defaultClient();
    }

    /**
     * Helper method to retrieve SSM Parameter's value
     * @param parameterName identifier of the SSM Parameter
     * @return decrypted parameter value
     */
    public String getParameter(String parameterName) {
        GetParameterRequest request = new GetParameterRequest();
        request.setName(parameterName);
        request.setWithDecryption(true);
        GetParameterResult result = ssm.getParameter(request);
        String parameterValue = result.getParameter().getValue();
        return parameterValue;
    }


    public static ParameterStore getInstance() {
        if(instance == null) {
            return new ParameterStore();
        }
        return instance;
    }
}

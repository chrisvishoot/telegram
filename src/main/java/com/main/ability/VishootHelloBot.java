package com.main.ability;

import com.main.aws.ParameterStore;
import com.main.constants.ParameterConstants;
import com.main.model.Contacts;
import com.main.model.TelegramUser;
import com.main.service.Service;
import com.main.service.ServiceImpl;
import org.springframework.http.ResponseEntity;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.objects.Locality;
import org.telegram.abilitybots.api.objects.MessageContext;
import org.telegram.abilitybots.api.objects.Privacy;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Arrays;

public class VishootHelloBot extends AbilityBot {
    private final static ParameterStore parameterStore = ParameterStore.getInstance();
    private static final String BOT_TOKEN = parameterStore.getParameter(ParameterConstants.BOT_TOKEN);
    private static final String BOT_USERNAME = parameterStore.getParameter(ParameterConstants.BOT_USERNAME);



    public VishootHelloBot() {
        super(BOT_TOKEN, BOT_USERNAME);
    }


    public Ability getReportForUser() {
        Service service = new ServiceImpl();
        return Ability
                .builder()
                .name("userreport")
                .info("userreport")
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    TelegramUser telegramUser = createTelegramUser(ctx);
                    ResponseEntity<Contacts[]> contacts = service.getReportForUser(telegramUser);
                    silent.send(Arrays.toString(contacts.getBody()), ctx.chatId());
                })
                .build();
    }


    public Ability parseText() {
        Service service = new ServiceImpl();
        return Ability
                .builder()
                .name("contacts")
                .info("contacts")
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    TelegramUser telegramUser = createTelegramUser(ctx);
                    String singleReport = ctx.update().getMessage().getText();
                    Contacts contacts = service.parseSingleReport(singleReport);
                    contacts.setIdTelegramUser(telegramUser.getIdTelegramUser());

                    boolean userExist = service.doesUserExist(telegramUser);
                    if(!userExist) {
                        //Create the user
                        service.createTelegramUser(telegramUser);
                    } else {
                        System.out.println("User already exists");
                    }
                    ResponseEntity<Contacts> contactUpdateResponse = service.createUserReport(contacts);
                    Contacts contactUpdate = contactUpdateResponse.getBody();
                    silent.send(telegramUser.toString(), ctx.chatId());
                    silent.send(contactUpdate.toString(), ctx.chatId());
                    silent.send("Finished Processing Request", ctx.chatId());

                })
                .build();
    }



    public TelegramUser createTelegramUser(MessageContext ctx) {
        TelegramUser telegramUser = new TelegramUser();
        Long userId = Long.valueOf(ctx.user().getId());
        String userName = ctx.user().getFirstName() + " " + ctx.user().getLastName();
        telegramUser.setIdTelegramUser(userId);
        telegramUser.setTelegramUserName(userName);
        return telegramUser;
    }

    @Override
    public int creatorId() {
        return 1337; // Your ID here
    }
}

package com.main.constants;

public class ParameterConstants {
    public static String BOT_TOKEN = "BOT_TOKEN";
    public static String BOT_USERNAME = "BOT_USERNAME";
    public static String DATABASE = "DATABASE_BASE_URL";
}

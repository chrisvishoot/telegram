package com.main.service;

import com.main.aws.ParameterStore;
import com.main.constants.ParameterConstants;
import com.main.model.Contacts;
import com.main.model.TelegramUser;
import com.main.parser.SingleReportParser;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.api.objects.Contact;

import java.util.ArrayList;
import java.util.List;


public class ServiceImpl implements Service {

    RestTemplate restTemplate = new RestTemplate();
    SingleReportParser singleReportParser = new SingleReportParser();
    ParameterStore parameterStore = ParameterStore.getInstance();
    String baseUrl = parameterStore.getParameter(ParameterConstants.DATABASE);

    public Contacts parseSingleReport(String report) {
        return singleReportParser.parse(report);
    }



    public ResponseEntity<Contacts[]> getReportForUser(TelegramUser telegramUser) {
        String getReportForUser = baseUrl + "/getReportForUser";
        ResponseEntity<Contacts[]> result = this.restTemplate.postForEntity(
                getReportForUser,
                telegramUser,
                Contacts[].class
        );
        return result;
    }

    public ResponseEntity<TelegramUser> createTelegramUser(TelegramUser telegramUser) {
        String createTelegramUser = baseUrl + "/createTelegramUser";
        ResponseEntity<TelegramUser> result = this.restTemplate.postForEntity(
                createTelegramUser,
                telegramUser,
                TelegramUser.class
        );
        return result;

    }

    public boolean doesUserExist(TelegramUser telegramUser) {
        boolean userExist = false;
        String userExistURL = baseUrl + "/checkIfUserExists";
        System.out.println(userExistURL);
        System.out.println(telegramUser.toString());

        try {
            ResponseEntity<TelegramUser> result = this.restTemplate.postForEntity(
                    userExistURL,
                    telegramUser,
                    TelegramUser.class
            );
            userExist = true;

        } catch(HttpClientErrorException ex) {
            userExist = false;
        }

        return userExist;
    }

    public ResponseEntity<Contacts> createUserReport(Contacts contacts) {
        String createReportEndPoint = baseUrl + "/createReport";
        ResponseEntity<Contacts> result = this.restTemplate.postForEntity(
                createReportEndPoint,
                contacts,
                Contacts.class
        );
        return result;
    }

    public String testGetEndPoint() {
        String result = "";
        String url = "https://api.telegram.org/bot937312215:AAFjAQB-EBk2TaFTpdHGXN3ZZwqfepuBQfI/getMe";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        HttpEntity<String> response = restTemplate.exchange(url,
                HttpMethod.GET,
                entity,
                String.class);
        result = response.getBody();
        System.out.println(result);
        return result;


    }
}

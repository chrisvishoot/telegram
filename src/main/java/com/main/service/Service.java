package com.main.service;

import com.main.model.Contacts;
import com.main.model.TelegramUser;
import org.springframework.http.ResponseEntity;

import java.util.List;

@org.springframework.stereotype.Service
public interface Service {
     String testGetEndPoint();
     Contacts parseSingleReport(String entireReport);
     ResponseEntity<Contacts> createUserReport(Contacts contacts);
     boolean doesUserExist(TelegramUser telegramUser);
     ResponseEntity<TelegramUser> createTelegramUser(TelegramUser telegramUser);
     ResponseEntity<Contacts[]> getReportForUser(TelegramUser telegramUser);

}

package com.main.parser;

import com.main.model.Contacts;

public interface Parser {
    public Contacts parse(String messageToParse);
}

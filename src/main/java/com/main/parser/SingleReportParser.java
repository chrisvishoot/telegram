package com.main.parser;

import com.main.model.Contacts;

public class SingleReportParser implements Parser {


    public Contacts parse(String messageToParse) {
        String[] splitByComma = messageToParse.split(",");
        Contacts contacts = new Contacts();
        for(String reportLine : splitByComma) {
            updateContacts(reportLine, contacts);
        }
        return contacts;
    }


    private void updateContacts(String reportLine, Contacts contacts) {
        reportLine = reportLine.toLowerCase();
        if(reportLine.contains("promised")) {
            Long promisedToGo = getNumberFromReport(reportLine);
            contacts.setPromisedToGo(promisedToGo);
        } else if(reportLine.contains("contacts")) {
            Long numberOfContacts = getNumberFromReport(reportLine);
            contacts.setNumberOfContacts(numberOfContacts);
        }
    }

    private Long getNumberFromReport(String reportLine) {
        String[] splitByColon = reportLine.split(":");
        Long reportNumber = Long.parseLong(splitByColon[1].trim());
        return reportNumber;
    }






}
